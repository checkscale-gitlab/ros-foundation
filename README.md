# ros-foundation

Foundation image used in Robot Operating System (ROS) docerized applications

# Features
*  [Based on OSRF's ROS docker images](https://hub.docker.com/_/ros)
*  [colcon build system](https://colcon.readthedocs.io/en/released/user/quick-start.html)
*  [Compatible with ros_gitlab_cicd](https://gitlab.com/robotwebservices/ros_gitlab_cicd)
*  Tested: [![pipeline status](https://gitlab.com/robotwebservices/docker_images/ros-foundation/badges/develop/pipeline.svg)](https://gitlab.com/robotwebservices/docker_images/ros-foundation/commits/develop)

# How to use
Simple example
```
docker run -it registry.gitlab.com/robotwebservices/ros-foundation:melodic roscore
```
Outputs:
```
... logging to /root/.ros/log/06be2230-d0c3-11e9-913b-0242ac110002/roslaunch-62acc4444f21-1.log
Checking log directory for disk usage. This may take awhile.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://62acc4444f21:41957/
ros_comm version 1.14.3


SUMMARY
========

PARAMETERS
 * /rosdistro: melodic
 * /rosversion: 1.14.3

NODES

auto-starting new master
process[master]: started with pid [38]
ROS_MASTER_URI=http://62acc4444f21:11311/

setting /run_id to 06be2230-d0c3-11e9-913b-0242ac110002
process[rosout-1]: started with pid [49]
started core service [/rosout]
```

# Testing
The ros-foundation docker images are tested by running the images and assert that rosmaster is running